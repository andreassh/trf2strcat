# trf2strcat.py

Convert Tandem Repeat Finder (TRF) DAT file to a STR tool catalogue file (BED/JSON). It is compatible with tools such as ExpansionHunter, lobSTR, GangSTR, HipSTR, RepeatSeq and GATK.

TRF tool can be downloaded from http://tandem.bu.edu/trf/trf.html

Acquire DAT file by running TRF with -h or -d flag, for example: ./trf409.linux64 /path/to/reference/file/hg19.fa 2 10 10 80 10 50 6 -h

Afterwards, run the script by using the following parameters:
--dat		input DAT file
--out		output BED/JSON file (use .json for ExpansionHunter and .bed for other tools)
--tool		name of the tool in which format the BED file will be created (options: expansionhunter, lobstr, gangstr, hipstr, repeatseq, gatk)

Example:
`python3 trf2strcat.py --dat hg19.fa.2.10.10.80.10.50.6.dat --out str_catalogue.json --tool expansionhunter`

